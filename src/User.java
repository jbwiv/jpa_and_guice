

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the usr database table.
 * 
 */
@Entity
@Table(name = "usr")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_no", unique = true, nullable = false)
    private Integer userNo;

    @Column(name = "first_name", length = 2147483647)
    private String firstName;

    @Column(name = "last_name", length = 2147483647)
    private String lastName;

    @Column(name = "mod_timestamp")
    private Timestamp modTimestamp;

    @Column(name = "user_password", length = 2147483647)
    private String userPassword;

    @Column(nullable = false, length = 2147483647)
    private String username;
    public User() {
    }

    public Integer getUserNo() {
        return this.userNo;
    }

    public void setUserNo(Integer userNo) {
        this.userNo = userNo;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Timestamp getModTimestamp() {
        return this.modTimestamp;
    }

    public void setModTimestamp(Timestamp modTimestamp) {
        this.modTimestamp = modTimestamp;
    }

    public String getUserPassword() {
        return this.userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}