

import com.google.inject.Inject;

import com.google.inject.persist.PersistService;

public class PersistenceInitializer {
    @Inject PersistenceInitializer(PersistService service) {
        System.out.println("Starting service...");
        service.start();
    }
}
