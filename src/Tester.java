import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;


class Tester {
        @Inject EntityManager em;
        @Transactional
        public void doAQuery () {
            Query q = em.createQuery("select t from User u");
            List<User> todoList = q.getResultList();
            for (User todo : todoList) {
              System.out.println(todo);
            }
            System.out.println("Size: " + todoList.size()); 
            em.close();
        }
    }