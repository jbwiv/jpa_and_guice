import com.google.inject.Guice;
import com.google.inject.Injector;


public class Main {   
    public static void main(String[] args) {
      Main t = new Main();
      Injector injector = Guice.createInjector(new PersistenceModule());
      injector.getInstance(PersistenceInitializer.class);
      System.out.println("Calling doAQuery...");
      new Tester().doAQuery();     
    }     
}
